//
//  Deck.m
//  Matchismo
//
//  Created by Richard Lo on 8/21/13.
//  Copyright (c) 2013 RLo Labs. All rights reserved.
//

#import "Deck.h"

@interface Deck()
@end

@implementation Deck

/*
 * Method: Card Getter
 *
 * Description:
 * Getter implemented using "lazy instantiation"
 */
- (NSMutableArray *)cards
{
    if (!_cards) { // Lazy Instantiation
        _cards = [[NSMutableArray alloc] init];
    }
    return _cards;
}

/*
 * Method: Add Card
 *
 * Description: 
 * Includes boolean 'atTop' which allows user to add a 
 * card to the top of the deck or append to end
 */
- (void)addCard:(Card *)card atTop:(BOOL)atTop
{
    if (card) {
        if (atTop) {
            [self.cards insertObject:card atIndex:0];
        } else {
            [self.cards addObject:card];
        }
    }
}

/*
 * Method: Draw Random Card
 *
 * Description:
 * Randomly pulls a card from the deck (array) and deletes 
 * that card from the deck
 */
- (Card *)drawRandomCard
{
    Card *randomCard;
    
    if (self.cards.count) {
        unsigned index = arc4random() % self.cards.count;
        randomCard = self.cards[index];
        [self.cards removeObjectAtIndex:index];
    }
    return randomCard;
}

@end
