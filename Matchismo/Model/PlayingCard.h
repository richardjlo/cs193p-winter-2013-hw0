//
//  PlayingCard.h
//  Matchismo
//
//  Created by Richard Lo on 8/21/13.
//  Copyright (c) 2013 RLo Labs. All rights reserved.
//

#import "Card.h"

@interface PlayingCard : Card

@property (strong, nonatomic) NSString *suit;
@property (nonatomic) NSUInteger rank;

+ (NSArray *)validSuits;
+ (NSUInteger)maxRank;

@end
