//
//  CardMatchingGame.m
//  Matchismo
//
//  Created by Richard Lo on 8/27/13.
//  Copyright (c) 2013 RLo Labs. All rights reserved.
//

#import "CardMatchingGame.h"

@interface CardMatchingGame()

@property (strong, nonatomic) NSMutableArray *cards; // of card
@property (readwrite, nonatomic) int totalScore;
@property (strong, nonatomic, readwrite) Card *card1;
@property (strong, nonatomic, readwrite) Card *card2;
@property (nonatomic) BOOL isSingle;
@property (nonatomic, readwrite) BOOL noMatch;


@end

@implementation CardMatchingGame

#define FLIP_COST 1
#define MISMATCH_PENALTY 2
#define MATCH_BONUS 4

// Lazy instantiation
- (NSMutableArray *)cards
{
    if (!_cards) _cards = [[NSMutableArray alloc] init];
    return _cards;
}

// Lazy instantiation
- (Card *)card1
{
    if (!_card1) _card1 = [[Card alloc] init];
    return _card1;
}

// Lazy instantiation
- (Card *)card2
{
    if (!_card2) _card2 = [[Card alloc] init];
    return _card2;
}

// Deck Initializer using: 1) card count & 2) certain deck
- (id)initWithCardCount:(NSUInteger)cardCount
              usingDeck:(Deck *)deck
{
    self = [super init];
    
    if (self) {
        for (int i = 0; i < cardCount; i++) {
            Card *card = [deck drawRandomCard];
            if (!card) {
                self = nil;
                break;
            } else {
                self.cards[i] = card;
            }
        }
    }
    return self;
    
}

// Returns a card at a given index
- (Card *)cardAtIndex:(NSUInteger)index
{
    if (index < [self.cards count]) {
        return self.cards[index];
    } else {
        return nil;
    }
}

- (void)flipCardAtIndex:(NSUInteger)index
{
    self.noMatch = FALSE;
    self.isSingle = TRUE;
    Card *emptyCard;
    self.card1 = emptyCard;
    self.card2 = emptyCard;
    Card *card = [self cardAtIndex:index];
    if(card && !card.isUnplayable) {
        if(!card.isFaceUp) {
            for (Card *otherCard in self.cards) {
                if (otherCard.isFaceUp && !otherCard.isUnplayable)   {
                    int matchScore = [card match:@[otherCard]]; //'@[]' makes an array
                    self.card1 = otherCard;
                    self.card2 = card;
                    self.isSingle = FALSE;
                    if (matchScore > 0) { // if there's a match
                        card.unplayable = YES; // make card1 unplayable
                        otherCard.unplayable = YES; // make card2 unplayable
                        self.score = matchScore * MATCH_BONUS;
                        self.totalScore += self.score;
                    } else {
                        otherCard.faceUp = NO; // turn card over
                        self.score = MISMATCH_PENALTY * (-1);
                        self.totalScore += self.score;
                    }
                    break;
                }
                
            }
            self.totalScore -= FLIP_COST;
        }
        if (self.isSingle && card.isFaceUp) {
//            NSLog(@"card isFaceUp %d", card.isFaceUp);
            self.card1 = card;
            self.noMatch = TRUE;
        }

        card.faceUp = !card.isFaceUp;
    }
}


- (void)startNewGame:(NSUInteger)cardCount usingDeck:(Deck *)deck   
{
    self.totalScore = 0;
    self.card1 = nil;
    self.card2 = nil;
    self.noMatch = NO;
    [self reinitDeck:deck];
}

- (void)reinitDeck:(Deck *)deck 
{
    /* Copy cards from array and add to top of deck */
    for (int i = 0; i < [self.cards count]; i++) {
        Card *card = [self.cards objectAtIndex:i];
        [deck addCard:card atTop:YES];
    }
    
    /* Redraw cards from randomized deck */
     for (int i = 0; i < [self.cards count]; i++) {
     Card *card = [deck drawRandomCard];
     self.cards[i] = card;
     }
    
    /* Flip all cards over and make them playable */
    for (Card *card in self.cards) {
        card.unplayable = NO;
        card.faceUp = NO;
    }
}


@end
