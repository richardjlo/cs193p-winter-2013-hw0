//
//  Deck.h
//  Matchismo
//
//  Created by Richard Lo on 8/21/13.
//  Copyright (c) 2013 RLo Labs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Card.h"

@interface Deck : NSObject


- (void)addCard:(Card *)card atTop:(BOOL)atTop;
@property (strong, nonatomic) NSMutableArray *cards; //of cards


- (Card *)drawRandomCard;

@end
